// NodeJS

// dependencies:
// npm install csv-parse
// npm install timsort

const fs = require("fs");
const parse = require('csv-parse/lib/sync');
const timsort = require('timsort');

var csvText = fs.readFileSync("data.csv")

/**
    @typedef {Object} Node -
    @property {number} id - id field
    @property {number} lat - latitude
    @property {number} lon - longitude
    @property {string} name - name of shop, whatever
    @property {string} amenity - type of shop, e.g. cafe
 */

/** @type {Node[]} */
var nodes = parse(csvText, {
    from: 2, // skip first line
    columns: function(columnNames) {
        return columnNames.map(function(p) {
            /** @type {string}  */
            let colName = p;
            if (colName[0] == '@') {
                return colName.substr(1);
            }
            return colName;
        })
    }
});

// for(var i = 0; i < 3; i++) {
//     let node = data[i];
//     console.log(node)
// }

/**
 *  @param {number} deg -
 *  @returns {number} -
 */
function radians(deg) {
    return deg * Math.PI / 180
}

function distance(lat1, lon1, lat2, lon2) {
    // based on python version taken from: https://stackoverflow.com/a/4913653/35364
    // convert decimal degrees to radians
    lat1 = radians(lat1)
    lat2 = radians(lat2)
    lon1 = radians(lon1)
    lon2 = radians(lon2)
    // haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = Math.pow(Math.sin(dlat/2), 2) + Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(dlon/2), 2)
    c = 2 * Math.asin(Math.sqrt(a))
    const r = 6371008 // Average radius of earth in meters
    return c * r
}

/**
 * @param {Node[]} node_list -
 * @param {number} lat -
 * @param {number} lon -
 * @param {number} dist -
 * @returns {Node[]}
 */
function nearby_range(node_list, lat, lon, dist) {
    /** @type {Node[]} */
    let res = [];
    for (let i = 0; i < node_list.length; i++) {
        let node = node_list[i];
        if (distance(lat, lon, node.lat, node.lon) < dist) {
            res.push(node);
        }
    }
    return res;
}

/**
 * @param {Node[]} node_list -
 * @param {number} lat -
 * @param {number} lon -
 * @param {number} offset -
 * @param {number} length -
 * @returns {Node[]}
 */
function nearby_sorted(node_list, lat, lon, offset, length) {
    var sorting_array = node_list.map(function(node) {
        return {
            dist: distance(lat, lon, node.lat, node.lon),
            node: node
        }
    });
    // sorting_array.sort(function(n) { return n.dist} ); // super slow!
    timsort.sort(sorting_array, function(n) { return n.dist });
    return sorting_array.slice(offset, offset+length).map(function(n) { return n.node });
}

/**
    @typedef {Object} Location -
    @property {number} lat -
    @property {number} lon -
    @property {string} name -
*/

/**
    @param {number} lat -
    @param {number} lon -
    @param {string} name -
    @returns {Location} -
*/
function make_loc(lat, lon, name) {
    return { lat, lon, name }
}

locations = [
    make_loc(35.6594599,139.6996217, "shibuya"),
    make_loc(35.6892477,139.7029394, "shinjuku"),
    make_loc(35.6987212,139.7737301, "akihabara"),

    make_loc(34.680032,135.5344016, "morinomiya"),
    make_loc(34.7006811,135.4999215, "osaka"),

    make_loc(34.9871881,135.7581169, "kyoto"),
    make_loc(34.9957857,135.7701609, "kiyomizu"),

    make_loc(35.1707314,136.8870091, "nagoya")
];

// filter by range
for (let loc of locations) {
    let t1 = Date.now();
    let result = nearby_range(nodes, loc.lat, loc.lon, 5000)
    let t2 = Date.now();
    console.log("" + result.length + " nodes nearby " + loc.name + "; time: " + (t2-t1) + "ms");
}

console.log();

// sort all by location
for (let loc of locations) {
    let t1 = Date.now();
    let sorted = nearby_sorted(nodes, loc.lat, loc.lon, 0, 30);
    let t2 = Date.now();
    console.log("top " + sorted.length + " nearby " + loc.name + "; time: " + (t2-t1) + "ms");
}