import csv
from typing import *

class Node(object):
    # node_id: int
    # lat: float
    # lon: float
    # name: str
    # amenity: str

    def __init__(self, node_id: int, lat: float, lon: float, name: str, amenity: str) -> None:
        self.node_id = node_id
        self.lat = lat
        self.lon = lon
        self.name = name
        self.amenity = amenity

    def __str__(self):
        return "{name} ({lat}, {lon}) #{amenity}".format(name=self.name, lat=self.lat, lon=self.lon, amenity=self.amenity)

node_list = []  # type: List[Node]

with open('data.csv', newline='') as csvfile:
    nodes = csv.reader(csvfile, delimiter=',', quotechar='"')
    count = 0
    skip = 2
    for node in nodes:
        count += 1
        if count <= skip: continue # note: <= because we increase count already
        node_id = int(node.pop(0))
        lat = float(node.pop(0))
        lon = float(node.pop(0))
        name = node.pop(0)
        amenity = node.pop(0)

        node_list.append(Node(node_id=node_id, lat=lat, lon=lon, name=name, amenity=amenity))

from math import radians, cos, sin, asin, sqrt
def distance(lat1: float, lon1: float, lat2: float, lon2: float) -> float:
    # taken from: https://stackoverflow.com/a/4913653/35364
    # convert decimal degrees to radians
    lon1 = radians(lon1)
    lon2 = radians(lon2)
    lat1 = radians(lat1)
    lat2 = radians(lat2)
    # haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a))
    r = 6371008 # Average radius of earth in meters
    return c * r

locations = [
    (35.6594599,139.6996217, "shibuya"),
    (35.6892477,139.7029394, "shinjuku"),
    (35.6987212,139.7737301, "akihabara"),

    (34.680032,135.5344016, "morinomiya"),
    (34.7006811,135.4999215, "osaka"),

    (34.9871881,135.7581169, "kyoto"),
    (34.9957857,135.7701609, "kiyomizu"),

    (35.1707314,136.8870091, "nagoya"),
]

# print("Parsed", len(node_list), "nodes")

from time import monotonic

def nearby_range(lat, lon, range) -> List[Node]:
    out = []
    for node in node_list:
        if distance(lat, lon, node.lat, node.lon) < range:
            out.append(node)
    return out

def nearby_sorted(lat, lon, offset, length) -> List[Node]:
    sorted_nodes = sorted(node_list, key=lambda node: distance(lat, lon, node.lat, node.lon))
    return sorted_nodes[offset:offset+length]

def ms(t):
    return format(t * 1000, ".2f") + "ms"

verbose_range = False
verbose_sorted = False

for lat, lon, name in locations:
    t1 = monotonic()
    result = nearby_range(lat, lon, 5000)
    t2 = monotonic()
    print(len(result), "nodes near " + name + "; time:", ms(t2-t1))

    if verbose_range:
        for node in result:
            print("\t", node, sep='')
        print()


print()

for lat, lon, name in locations:
    t1 = monotonic()
    result = nearby_sorted(lat, lon, 0, 30)
    t2 = monotonic()
    print("top 30 nearby", name, "sorted; time:", ms(t2-t1))
    if verbose_sorted:
        for node in result:
            print("\t", node, sep='')
        print()

