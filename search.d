import std.stdio;
import std.csv;
import std.file;
import std.typecons;
import std.conv;

class Node {
    ulong id;
    double lat;
    double lon;
    string name;
    string amenity;

    string to_str() {
        return name ~ " (" ~ lat.to!string ~ ", " ~ lon.to!string ~ ") #" ~ amenity;
    }
}

import std.math;

double radians(double deg) {
    return deg * PI / 180;
}

double distance(double lat1, double lon1, double lat2, double lon2) {
    // based on python version taken from: https://stackoverflow.com/a/4913653/35364
    // convert decimal degrees to radians
    lat1 = radians(lat1);
    lat2 = radians(lat2);
    lon1 = radians(lon1);
    lon2 = radians(lon2);
    // haversine formula
    auto dlon = lon2 - lon1;
    auto dlat = lat2 - lat1;
    auto a = pow(sin(dlat/2), 2) + cos(lat1) * cos(lat2) * pow(sin(dlon/2), 2);
    auto c = 2 * asin(sqrt(a));
    const r = 6371008; // Average radius of earth in meters
    return c * r;
}

Node[] node_list;

struct Location {
    double lat;
    double lon;
    string name;
}

Node[] nearby_range(double lat, double lon, double range) {
    Node[] res;
    foreach(node; node_list) {
        if(distance(lat, lon, node.lat, node.lon) < range) {
            res ~= node;
        }
    }
    return res;
}

Node[] nearby_sorted(double lat, double lon, int offset, int length) {
    import std.algorithm;
    auto sorted_nodes = schwartzSort!(n => distance(lat, lon, n.lat, n.lon), "a < b")(node_list);
    ulong index = 0;
    Node[] res;
    int end = offset + length;
    foreach(node; sorted_nodes) {
        if(index >= end) {
            break;
        }
        if(index >= offset) {
            res ~= node;
        }
        index++;
    }
    return res;
}

auto locations = [
    Location(35.6594599,139.6996217, "shibuya"),
    Location(35.6892477,139.7029394, "shinjuku"),
    Location(35.6987212,139.7737301, "akihabara"),

    Location(34.680032,135.5344016, "morinomiya"),
    Location(34.7006811,135.4999215, "osaka"),

    Location(34.9871881,135.7581169, "kyoto"),
    Location(34.9957857,135.7701609, "kiyomizu"),

    Location(35.1707314,136.8870091, "nagoya"),
];

import core.time;
alias monotonic = MonoTime.currTime;

string ms(Duration d) {
    return (d.total!"usecs"() / 1000.0).to!string ~ "ms";
}

bool verbose_range = false;
bool verbose_sorted = false;

void main() {
    auto csvLines = csvReader!string(readText("data.csv"));
    const skip = 2;
    auto lineNo = 0;
    foreach(line; csvLines) {
        lineNo += 1;
        // if (lineNo < 10) writeln(line);
        if (lineNo <= skip) continue;
        string[] parts;
        foreach(part; line) {
            parts ~= part;
        }
        auto node = new Node;
        node.id = parts[0].parse!ulong;
        node.lat = parse!double(parts[1]);
        node.lon = parts[2].parse!double;
        node.name = parts[3];
        node.amenity = parts[4];
        node_list ~= node;
        // if (lineNo < 10) writeln(node.id, " ", node.name, " (",node.lat,", ",node.lon,")");
    }
    // writeln(node_list.length);

    // nearby range
    foreach(location; locations) {
        auto t1 = monotonic();
        auto result = nearby_range(location.lat, location.lon, 5000);
        auto time = monotonic() - t1;
        writeln(result.length, " nodes near ", location.name, "; time: ", time.ms);
        if(verbose_range) {
            foreach(node; result) {
                writeln("\t", node.to_str());
            }
            writeln();
        }
    }

    writeln();

    foreach(location; locations) {
        auto t1 = monotonic();
        auto result = nearby_sorted(location.lat, location.lon, 0, 30);
        auto time = monotonic() - t1;
        writeln("top 30 nearby ", location.name, " sorted; time: ", time.ms);
        if(verbose_sorted) {
            foreach(node; result) {
                writeln("\t", node.to_str());
            }
            writeln();
        }
    }

}