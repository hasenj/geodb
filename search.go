package main

import (
	"encoding/csv"
	"fmt"
	"math"
	"os"
	"sort"
	"strconv"
	"time"
)

type Node struct {
	id int

	lat, lon float64

	name, amenity string
}

func toString(f float64, prec int) string {
	return strconv.FormatFloat(f, 'f', prec, 64)
}

func (node *Node) String() string {
	return node.name + " (" + toString(node.lat, 2) + ", " + toString(node.lon, 2) + ") #" + node.amenity
}

type Location struct {
	lat, lon float64

	name string
}

func main() {
	file, _ := os.Open("data.csv")
	csv := csv.NewReader(file)
	// skip the first two lines
	csv.Read() // headers
	count_line, _ := csv.Read()
	count, _ := strconv.Atoi(count_line[len(count_line)-1])
	nodes := make([]Node, 0, count)
	for {
		fields, _ := csv.Read()
		if fields == nil { // file ended
			break
		}
		id, _ := strconv.Atoi(fields[0])
		lat, _ := strconv.ParseFloat(fields[1], 64)
		lon, _ := strconv.ParseFloat(fields[2], 64)
		name := fields[3]
		amenity := fields[4]
		node := Node{
			id:      id,
			lat:     lat,
			lon:     lon,
			name:    name,
			amenity: amenity,
		}
		nodes = append(nodes, node)
	}
	// fmt.Println(len(nodes))
	// fmt.Println(nodes[4])

	locations := []Location{
		Location{35.6594599, 139.6996217, "shibuya"},
		Location{35.6892477, 139.7029394, "shinjuku"},
		Location{35.6987212, 139.7737301, "akihabara"},

		Location{34.680032, 135.5344016, "morinomiya"},
		Location{34.7006811, 135.4999215, "osaka"},

		Location{34.9871881, 135.7581169, "kyoto"},
		Location{34.9957857, 135.7701609, "kiyomizu"},

		Location{35.1707314, 136.8870091, "nagoya"},
	}

	const verbose_range = false
	const verbose_sorted = false

	for _, loc := range locations {
		t1 := time.Now()
		result := nearby_range(nodes, loc.lat, loc.lon, 5000)
		dur := time.Since(t1)
		fmt.Println(len(result), "nodes nearby", loc.name+"; time:", dur)
	}

	fmt.Println()
	for _, loc := range locations {
		t1 := time.Now()
		result := nearby_sorted(nodes, loc.lat, loc.lon, 0, 30)
		dur := time.Since(t1)
		fmt.Println("top 30 nearby", loc.name+" sorted; time:", dur)
		if verbose_sorted {
			for _, n := range result {
				fmt.Println("\t" + n.String())
			}
		}
	}
}

func radians(deg float64) float64 {
	return deg * math.Pi / 180
}

func distance(lat1, lon1, lat2, lon2 float64) float64 {
	// based on python version taken from: https://stackoverflow.com/a/4913653/35364
	// convert decimal degrees to radians
	lat1 = radians(lat1)
	lat2 = radians(lat2)
	lon1 = radians(lon1)
	lon2 = radians(lon2)
	// haversine formula
	dlon := lon2 - lon1
	dlat := lat2 - lat1
	a := math.Pow(math.Sin(dlat/2), 2) + math.Cos(lat1)*math.Cos(lat2)*math.Pow(math.Sin(dlon/2), 2)
	c := 2 * math.Asin(math.Sqrt(a))
	const r = 6371008 // Average radius of earth in meters
	return c * r
}

func nearby_range(node_list []Node, lat, lon, dist float64) []Node {
	res := make([]Node, 0)
	for _, node := range node_list {
		if distance(lat, lon, node.lat, node.lon) < dist {
			res = append(res, node)
		}
	}
	return res
}

type NodeSortWrapper struct {
	distance float64
	node     Node
}

type SortableNodes []NodeSortWrapper

func (a SortableNodes) Len() int { return len(a) }

func (a SortableNodes) Swap(i, j int) { a[i], a[j] = a[j], a[i] }

func (a SortableNodes) Less(i, j int) bool { return a[i].distance < a[j].distance }

func nearby_sorted(node_list []Node, lat, lon float64, offset, limit int64) []Node {
	var res = make([]NodeSortWrapper, 0, len(node_list))
	for _, node := range node_list {
		distance := distance(lat, lon, node.lat, node.lon)
		res = append(res, NodeSortWrapper{node: node, distance: distance})
	}
	sort.Sort(SortableNodes(res))
	var output = make([]Node, 0, limit)
	for _, node := range res[offset : offset+limit] {
		output = append(output, node.node)
	}
	return output
}
